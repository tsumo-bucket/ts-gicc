<script>
    var imageDataContainer = "";

    $(".submit-form-btn").click(function() {
    	var isValid = true;
    	var errorMessage = "";
    	var type = $("[name=type]").val();

        $("[required]").prop("required", false);
        
        if (type == "default") {
            if(!$("[name=title]").val() || !$("[name=description]").val()) {
                
                if(!$("[name=title]").val())
                    $("[name=title]").addClass("invalid");

                if(!$("[name=description]").val())
                    $("[name=description]").addClass("invalid");

                errorMessage = "Please enter atleast a title or description.";
                isValid = false;
            }  
        } else if (type == "image") {
            if ($("[name=image]").val() == 0) {
                isValid = false;
                errorMessage = "Please select an image to display.";
            }
            console.log($("[name=image]").val());
        }
    	

    	if(!isValid) {
    		showNotifyToaster('error', '', errorMessage);
    	} else {
    		$(".form-parsley").submit();
    	}
    });

</script>